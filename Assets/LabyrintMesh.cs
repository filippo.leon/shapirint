﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;




public class LabyrintMesh : MonoBehaviour {
    public float tileInnerSize = 0.8f;
    public float tileBorderSize = 0.1f;
    public float borderHeight = 1.0f;

    class MeshConstructor {
        public List<Vector3> newVertices = new List<Vector3>();
        public List<Vector3> newNormals = new List<Vector3>();
        public List<Vector2> newUV = new List<Vector2>();
        public List<int> newTriangles = new List<int>();
    }

    void MakeQuad(MeshConstructor mc, Vector2 minB, Vector2 maxB, float h) {
        int v = mc.newVertices.Count;
        mc.newTriangles.Add(v + 0);
        mc.newTriangles.Add(v + 1);
        mc.newTriangles.Add(v + 2);
        mc.newTriangles.Add(v + 2);
        mc.newTriangles.Add(v + 1);
        mc.newTriangles.Add(v + 3);

        mc.newVertices.Add(new Vector3(minB.x, minB.y, -h));
        mc.newVertices.Add(new Vector3(minB.x, maxB.y, -h));
        mc.newVertices.Add(new Vector3(maxB.x, minB.y, -h));
        mc.newVertices.Add(new Vector3(maxB.x, maxB.y, -h));

        mc.newNormals.Add(new Vector3(0.0f, 0.0f, -1.0f));
        mc.newNormals.Add(new Vector3(0.0f, 0.0f, -1.0f));
        mc.newNormals.Add(new Vector3(0.0f, 0.0f, -1.0f));
        mc.newNormals.Add(new Vector3(0.0f, 0.0f, -1.0f));

        mc.newUV.Add(new Vector2(0.0f, 0.0f));
        mc.newUV.Add(new Vector2(1.0f, 0.0f));
        mc.newUV.Add(new Vector2(0.0f, 1.0f));
        mc.newUV.Add(new Vector2(1.0f, 1.0f));
    }

    void MakeVerticalQuad(MeshConstructor mc, Vector2 poleA, Vector2 poleB, Vector3 normal, float h) {
        int v = mc.newVertices.Count;
        mc.newTriangles.Add(v + 0);
        mc.newTriangles.Add(v + 1);
        mc.newTriangles.Add(v + 2);
        mc.newTriangles.Add(v + 2);
        mc.newTriangles.Add(v + 1);
        mc.newTriangles.Add(v + 3);

        mc.newVertices.Add(new Vector3(poleA.x, poleA.y, 0));
        mc.newVertices.Add(new Vector3(poleB.x, poleB.y, 0));
        mc.newVertices.Add(new Vector3(poleA.x, poleA.y, -h));
        mc.newVertices.Add(new Vector3(poleB.x, poleB.y, -h));

        mc.newNormals.Add(normal);
        mc.newNormals.Add(normal);
        mc.newNormals.Add(normal);
        mc.newNormals.Add(normal);

        mc.newUV.Add(new Vector2(0.0f, 0.0f));
        mc.newUV.Add(new Vector2(1.0f, 0.0f));
        mc.newUV.Add(new Vector2(0.0f, 1.0f));
        mc.newUV.Add(new Vector2(1.0f, 1.0f));
    }

    void MakeCell(MeshConstructor mc, Vector2 center, List<bool> arr) {

        Vector2 up = new Vector2(0.0f, tileInnerSize / 2.0f + tileBorderSize / 2.0f);
        Vector2 right = new Vector2(tileInnerSize / 2.0f + tileBorderSize / 2.0f, 0.0f);
        // Border
        MakeQuad(mc,
            center + up - new Vector2(tileInnerSize / 2, tileBorderSize / 2),
            center + up + new Vector2(tileInnerSize / 2, tileBorderSize / 2),
            arr[0] ? 0.0f : borderHeight
        );
        MakeQuad(mc,
            center - up - new Vector2(tileInnerSize / 2, tileBorderSize / 2),
            center - up + new Vector2(tileInnerSize / 2, tileBorderSize / 2),
            arr[1] ? 0.0f : borderHeight
        );
        MakeQuad(mc,
            center - right - new Vector2(tileBorderSize / 2, tileInnerSize / 2),
            center - right + new Vector2(tileBorderSize / 2, tileInnerSize / 2),
            arr[2] ? 0.0f : borderHeight
        );
        MakeQuad(mc,
            center + right - new Vector2(tileBorderSize / 2, tileInnerSize / 2),
            center + right + new Vector2(tileBorderSize / 2, tileInnerSize / 2),
            arr[3] ? 0.0f : borderHeight
        );
        // Border conn
        if (!arr[0]) {
            MakeVerticalQuad(mc,
                center + new Vector2(tileInnerSize / 2, tileInnerSize / 2),
                center + new Vector2(-tileInnerSize / 2, tileInnerSize / 2),
                new Vector3(0.0f, 1.0f, 0.0f),
                borderHeight
            );
        } else {
            MakeVerticalQuad(mc,
                center + new Vector2(tileInnerSize / 2, tileInnerSize / 2),
                center + new Vector2(tileInnerSize / 2, tileInnerSize / 2 + tileBorderSize),
                new Vector3(-1.0f, 0.0f, 0.0f),
                borderHeight
            );
            MakeVerticalQuad(mc,
                center + new Vector2(-tileInnerSize / 2, tileInnerSize / 2 + tileBorderSize),
                center + new Vector2(-tileInnerSize / 2, tileInnerSize / 2),
                new Vector3(1.0f, 0.0f, 0.0f),
                borderHeight
            );
        }
        if (!arr[1]) {
            MakeVerticalQuad(mc,
                center + new Vector2(-tileInnerSize / 2, -tileInnerSize / 2),
                center + new Vector2(tileInnerSize / 2, -tileInnerSize / 2),
                new Vector3(0.0f, 1.0f, 0.0f),
                borderHeight
            );
        } else {
            MakeVerticalQuad(mc,
                center + new Vector2(tileInnerSize / 2, - tileInnerSize / 2 - tileBorderSize),
                center + new Vector2(tileInnerSize / 2, - tileInnerSize / 2),
                new Vector3(-1.0f, 0.0f, 0.0f),
                borderHeight
            );
            MakeVerticalQuad(mc,
                center + new Vector2(-tileInnerSize / 2, -tileInnerSize / 2),
                center + new Vector2(-tileInnerSize / 2, -tileInnerSize / 2 - tileBorderSize),
                new Vector3(1.0f, 0.0f, 0.0f),
                borderHeight
            );
        }
        if (!arr[2]) { // Left not conn
            MakeVerticalQuad(mc,
                center + new Vector2(-tileInnerSize / 2, tileInnerSize / 2),
                center + new Vector2(-tileInnerSize / 2, -tileInnerSize / 2),
                new Vector3(1.0f, 0.0f, 0.0f),
                borderHeight
            );
        } else {
            MakeVerticalQuad(mc,
                center + new Vector2(-tileInnerSize / 2, tileInnerSize / 2),
                center + new Vector2(-tileInnerSize / 2 - tileBorderSize, tileInnerSize / 2),
                new Vector3(0.0f, 1.0f, 0.0f),
                borderHeight
            );
            MakeVerticalQuad(mc,
                center + new Vector2(-tileInnerSize / 2 - tileBorderSize, -tileInnerSize / 2),
                center + new Vector2(-tileInnerSize / 2, - tileInnerSize / 2),
                new Vector3(0.0f, 1.0f, 0.0f),
                borderHeight
            );
        }
        if (!arr[3]) { // Right not conn
            MakeVerticalQuad(mc,
                center + new Vector2(tileInnerSize / 2, -tileInnerSize / 2),
                center + new Vector2(tileInnerSize / 2, tileInnerSize / 2),
                new Vector3(-1.0f, 0.0f, 0.0f),
                borderHeight
            );
        } else {
            MakeVerticalQuad(mc,
                center + new Vector2(tileInnerSize / 2 + tileBorderSize, tileInnerSize / 2),
                center + new Vector2(tileInnerSize / 2, tileInnerSize / 2),
                new Vector3(0.0f, 1.0f, 0.0f),
                borderHeight
            );
            MakeVerticalQuad(mc,
                center + new Vector2(tileInnerSize / 2, -tileInnerSize / 2),
                center + new Vector2(tileInnerSize / 2 + tileBorderSize, -tileInnerSize / 2),
                new Vector3(0.0f, 1.0f, 0.0f),
                borderHeight
            );
        }
        // Corners
        MakeQuad(mc,
            center + new Vector2(tileInnerSize / 2, tileInnerSize / 2),
            center + new Vector2(tileInnerSize / 2 + tileBorderSize, tileInnerSize / 2 + tileBorderSize),
            borderHeight
        );
        MakeQuad(mc,
            center + new Vector2(-tileInnerSize / 2 - tileBorderSize, tileInnerSize / 2),
            center + new Vector2(-tileInnerSize / 2, tileInnerSize / 2 + tileBorderSize),
            borderHeight
        );
        MakeQuad(mc,
            center + new Vector2(tileInnerSize / 2, -tileInnerSize / 2 - tileBorderSize),
            center + new Vector2(tileInnerSize / 2 + tileBorderSize, -tileInnerSize / 2),
            borderHeight
        );
        MakeQuad(mc,
            center - new Vector2(tileInnerSize / 2 + tileBorderSize, tileInnerSize / 2 + tileBorderSize),
            center - new Vector2(tileInnerSize / 2, tileInnerSize / 2),
            borderHeight
        );
        // Inner
        MakeQuad(mc,
            center - new Vector2(tileInnerSize / 2, tileInnerSize / 2),
            center + new Vector2(tileInnerSize / 2, tileInnerSize / 2),
            0.0f
        );

    }

    void Start() {
        Labyrint labyrint = new Labyrint();

        MeshConstructor mc = new MeshConstructor();
        for (int i = 0; i < labyrint.size.x; ++i) {
            for (int j = 0; j < labyrint.size.y; ++j) {
                Vector2 here = new Vector2(i, j);
                Labyrint.Node node = labyrint.GetNode(new Labyrint.Point(i, j));
                List<bool> conn = new List<bool> { node.up, node.down, node.left, node.right };
                MakeCell(mc, here, conn);
            }
        }

        Mesh mesh = new Mesh();
        
        GetComponent<MeshFilter>().mesh = mesh;
        mesh.vertices = mc.newVertices.ToArray();
        mesh.uv = mc.newUV.ToArray();
        mesh.triangles = mc.newTriangles.ToArray();
        mesh.normals = mc.newNormals.ToArray();
    }

    void Update() {
        
    }
}
