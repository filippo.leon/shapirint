﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Labyrint {
    public struct Point {
        public Point(int x1, int y1) {
            x = x1;
            y = y1;
        }

        public int x, y;
    }

    public class Node {
        public bool up = false, down = false, left = false, right = false;
        public bool visited = false;
    }

    public Node[,] map;
    public Point size;

    enum Direction { Up, Down, Left, Right };

    Direction GetRandomDirection() {
        int rnd = Random.Range(0, 4);
        return (Direction)rnd;
    }

    bool IsValid(Point p) {
        return p.x >= 0 && p.y >= 0 && p.x < size.x && p.y < size.y;
    }

    Point GetNeighbour(Point p, Direction d) {
        switch (d) {
            case Direction.Up:
                return new Point(p.x, p.y+1);
            case Direction.Down:
                return new Point(p.x, p.y-1);
            case Direction.Left:
                return new Point(p.x-1, p.y);
            case Direction.Right:
                return new Point(p.x+1, p.y);
        }
        return new Point(-1, -1);
    }

    Point GetNextUnvisited(Point p, Direction dir, out Direction newDir) {
        newDir = dir;
        for (int i = 0; i < 4; ++i) {
            newDir = (Direction)((i + (int)dir) % 4);
            Point nhb = GetNeighbour(p, newDir);
            if (IsValid(nhb)) {
                Node nd = GetNode(nhb);
                Debug.Log(nd != null);
                if (nd != null && !nd.visited) {
                    return nhb;
                }
            }
        }
        return new Point(-1, -1);
    }

    void Link(Point from, Point to, Direction d) {
        switch(d) {
            case Direction.Up:
                GetNode(from).up = true;
                GetNode(to).down = true;
                break;
            case Direction.Down:
                GetNode(from).down = true;
                GetNode(to).up = true;
                break;
            case Direction.Left:
                GetNode(from).left = true;
                GetNode(to).right = true;
                break;
            case Direction.Right:
                GetNode(from).right = true;
                GetNode(to).left = true;
                break;
        }
    }

    public Node GetNode(Point p) {
        return map[p.x, p.y];
    }

    void Visit(Point p) {
        GetNode(p).visited = true;
    }

    public static float Map(float value, float from1, float to1, float from2, float to2) {
        return (value - from1) / (to1 - from1) * (to2 - from2) + from2;
    }

    public Labyrint() {
        size = new Point(30, 30);
        map = new Node[size.x, size.y];
        for (int i = 0; i < size.x; ++i) {
            for (int j = 0; j < size.y; ++j) {
                map[i, j] = new Node();
            }
        }

        Point start = new Point(17, 0);
        GetNode(start).visited = true;

        Queue<Point> branches = new Queue<Point>();
        branches.Enqueue(start);

        while (branches.Count > 0) {
            Point current = branches.Dequeue();
            while (true) {
                Direction dir = GetRandomDirection();
                Direction newDir;
                Point next = GetNextUnvisited(current, dir, out newDir);
                if (next.x < 0) {
                    break;
                } else {
                    Link(current, next, newDir);
                    branches.Enqueue(current);
                    GetNode(next).visited = true;
                    current = next;
                }
            }
        }

    }

    void Print() {        
        Debug.Log(string.Format("Done! Bye..."));

        // Debug.DrawPoint();
        // DrawLine(Vector3 start, Vector3 end, Color color = Color.white, float duration = 0.0f, bool depthTest = true);
        for (int i = 0; i < size.x; ++i) {
            for (int j = 0; j < size.y; ++j) {
                Vector3 here = new Vector3(i, j, 0);
                if (map[i, j].up) {
                    Debug.DrawLine(here, new Vector3(i, j + 1, 0), Color.red, 1000);
                    Debug.Log(string.Format("-> U {0} {1}", i, j));
                }
                if (map[i, j].down) {
                    Debug.DrawLine(here, new Vector3(i, j - 1, 0), Color.red, 1000);
                    Debug.Log(string.Format("-> D {0} {1}", i, j));
                }
                if (map[i, j].right) {
                    Debug.DrawLine(here, new Vector3(i + 1, j, 0), Color.red, 1000);
                    Debug.Log(string.Format("-> R {0} {1}", i, j));
                }
                if (map[i, j].left) {
                    Debug.DrawLine(here, new Vector3(i - 1, j, 0), Color.red, 1000);
                    Debug.Log(string.Format("-> L {0} {1}", i, j));
                }
            }
        }
    }
}
